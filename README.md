<!--
vim: spelllang=fr keymap=accents
-->
# Exercice 6 de programmation pour STM32 : Introduction aux pointeurs et aux références

En C, il existe la notion de variables, pointeurs et références. Les pointeurs
et le références permettent d'exprimer certaines instructions avec moins de code
et permettent des fonctions qui ne sont pas possibles avec des variables
standards.

## Marches à suivre

### Téléchargement
Exécutez les commandes suivantes dans *Git Bash*.
1. Créez un dossier pour le répertoire:
	```
	mkdir -p ~/code/grum/eurobot2019/exercices/exercices-pour-stm32
	```
1. Clonez le répertoire:
	```
	git clone ssh://gitlab@gitlab.robitaille.host:49/GRUM/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-6-pointers-et-references.git ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-6-pointers-et-references
	```
1. Ouvrez le dossier:
	```
	cd ~/code/GRUM/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-6-pointers-et-references
	```
1. Ouvrez le dossier dans *Visual Sutdio Code*:
	```
	code .
	```

### Programmation
Ouvrez le fichier `src/main.cpp` et suivez les directives.

### Exécution local
#### Compilation
Dans git bash:
```
pio run -e windows_x86
```

#### Exécution
```
.pioenvs/windows_x86/program.exe
```

### Exécution sur un microcontrôleur
#### Compilation
Dans git bash:
```
pio run -e nucleo_f303k8
```

#### Téléversement
```
pio run -e nucleo_f303k8 -t upload
```
