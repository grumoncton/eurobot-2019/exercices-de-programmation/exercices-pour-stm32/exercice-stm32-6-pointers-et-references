// vim: spelllang=fr, keymap=accents

#include <mbed.h>

Serial ser(USBTX, USBRX, NULL, 9600);


// En C, il existe la notion de variables, pointeurs et références. Les
// pointeurs et le références permettent d'exprimer certaines instructions avec
// moins de code et permettent des fonctions qui ne sont pas possibles avec des
// variables standards.

int main() {


	/**
	 * Partie A: Variables
	 *
	 * 1. Les variables en C (et tous autres langages de programmation) sont stockées
	 *    dans le mémoire (RAM) de l'ordinateur.
	 * 2. Le mémoire est adressé. C'est-à-dire, chaque octet dans le mémoire est
	 *    donne un identificateur unique.
	 * 3. En C, une variable est simplement une adresse mémoire.
	 */

	ser.printf("\nPartie A\n\n");

	// Définir une variable avec votre âge


	// Imprimer votre âge


	// Imprimer l'adresse de la variable qui contient votre âge


	// Incrémenter votre âge


	// Imprimer votre âge encore


	// Imprimer l'adresse de la variable qui contient votre âge encore


	// Définir un champs de caractères avec le contenu "Hello World!"


	// Imprimer le message


	// Imprimer l'adresse de la variable qui contient le message



	/**
	 * Partie B: Pointeurs
	 *
	 * Les pointeurs sont des variables qui contiennent l'adresse d'une autre
	 * variable.
	 */

	ser.printf("\nPartie B\n\n");

	// Définir un pointeur à l'âge


	// Imprimer la valeur du pointeur


	// Imprimer l'adresse du pointeur


	// Imprimer la valeur de la variable adressée par le pointeur


	// Définir un nouveau âge


	// Donner l'adresse du nouveau âge au pointeur


	// Imprimer encore une fois la valeur du pointer


	// Imprimer encore une fois l'adresse du pointer


	// Imprimer encore une fois la valeur du pointeur


	// Modifier via le pointeur la valeur dans la variable adressée par le
	// pointeur


	// Imprimer la valeur du pointeur


	// Imprimer la valeur de la variable adressée par le pointeur


	// Définir un pointeur au message


	// Imprimer la valeur du pointeur


	// Imprimer le message via le pointeur


	// Imprimer chaque caractère du message via le pointeur





	// Définir un pointeur nul
	// https://www.geeksforgeeks.org/few-bytes-on-null-pointer-in-c/


	// Imprimer la valeur du pointeur


	// Imprimer l'adresse du pointeur


	// Imprimer la valeur adressée par le pointeur



	/**
	 * Partie C: Références
	 *
	 * Les références et les pointeurs sont très semblables en C. Les références
	 * sont implémentés avec des pointeurs.
	 *
	 * Il est utile de penser à une référence comme un pointeur constant qui donne
	 * automatiquement sa valeur, pas son adresse (pas besoin d'utiliser `*`).
	 */

	ser.printf("\nPartie C\n\n");

	// Définir une référence à la variable contenant l'âge


	// Imprimer l'âge via la référence


	// Imprimer l'adresse de la variable contenant l'âge ainsi que l'adresse de la
	// référence


	// Modifier directement la valeur de la variable via la référence


	// Imprimer l'âge


	// Utiliser une variable pour modifier la valeur de la référence


	// Imprimer l'âge encore une fois


	// Imprimer encore une fois l'adresse de la variable contenant l'âge ainsi que
	// l'adresse de la référence


	return 0;
}
